---
title: "Making changes to the SDK"
weight: 1
infoType: task
icon: task
---
The best way to support and contribute to the project is to make actual changes and improvements to Freedesktop SDK.

<!--more-->

The recommended contribution workflow follows a standard forking workflow:

1. [Fork the Freedesktop SDK repository]({{< ref "/contributing/making-changes.md#forking-the-freedesktop-sdk-repository" >}}).
2. [Clone the fork of the Freedesktop SDK repository]({{< ref "/contributing/making-changes.md#cloning-the-freedesktop-sdk-repository" >}}).
3. [Create a local feature branch in the cloned repository]({{< ref "/contributing/making-changes.md#creating-a-local-branch" >}}).
4. [Commit and push your changes to the remote repository]({{< ref "/contributing/making-changes.md#pushing-your-changes-to-the-remote-repository" >}}).
5. [Create a merge request from the fork to the original Freedesktop SDK repository]({{< ref "/contributing/making-changes.md#creating-a-merge-request" >}}).

{{<tip "Prerequisites">}}

- Make sure you have a [GitLab account](https://gitlab.com/users/sign_up).
- Install and configure your local Git environment.
- Install [BuildStream 2](https://buildstream.build/).
    - For more information about using BuildStream, see the [BuildStream documentation](https://docs.buildstream.build/2.1/index.html).
- If you want to test changes locally, install [GNU Make](https://www.gnu.org/software/make/).
{{</tip>}}

## Forking the Freedesktop SDK repository

If you are a new contributor to the project, the best way to start contributing is to fork the [Freedesktop SDK repository](https://gitlab.com/freedesktop-sdk/freedesktop-sdk).
By creating your own fork of the project repository, you can make and commit changes without affecting the upstream project. When you are ready, you can create a merge request to submit your changes for review.

For more information about forking a GitLab repository, see the [GitLab documentation](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html).

- To contribute to Freedesktop SDK, fork the Freedesktop SDK repository from [gitlab.com/freedesktop-sdk/freedesktop-sdk](https://gitlab.com/freedesktop-sdk/freedesktop-sdk).
- To contribute to the Freedesktop SDK documentation, fork the Freedesktop SDK documentation repository from [gitlab.com/freedesktop-sdk/documentation](https://gitlab.com/freedesktop-sdk/documentation).

## Cloning the Freedesktop SDK repository

After you create a fork of the Freedesktop SDK repository, clone the Freedesktop SDK repository to your machine.

**To clone the fork of the Freedesktop SDK repository, follow these steps:**

1. Open a terminal of your choice.
2. At the command prompt, associate Git with your name and email address:

   ```shell
   git config user.name "MY_NAME"
   git config user.email "MY_EMAIL@example.com
   ```

3. Choose how to clone the Freedesktop SDK repository:
   - To clone using HTTPS, enter the following command:

        ```shell
        git clone https://gitlab.com/project-namespace/project-slug.git
        ```

   - To clone using SSH, enter the following command:

        ```shell
        git clone git@gitlab.com:project-namespace/project-slug.git
        ```

        {{<tip "Tip">}}
With the SSH connection, you don't have to provide your username and password each time you commit a change to the repository.
        {{</tip>}}

## Creating a local branch

Follow Git best practices and create a local feature branch to make and test changes on the cloned Freedesktop SDK repository.

**To create a local branch, follow these steps:**

1. Open your preferred terminal and go to the cloned repository.
2. At the command prompt, select the branch that you want to use as base for your changes:
   - To base your branch on the current development branch, enter the following command:
  
        ```shell
        git checkout master
        ```

   - To base your branch on a specific release, enter the following command:
  
        ```shell
        git checkout specific-release-branch
        ```

3. Create your feature branch by entering the following command:

    ```shell
        git checkout -b username/my-branch-name
     ```

    {{<note "Note">}}
 In `username/my-branch-name`, replace `username` with your actual GitLab username and `my-branch-name` with the name for your branch.
 The name of a branch should mirror your changes, for example `jsmith/fix-iris-driver-issue`.
    {{</note>}}

## Pushing your changes to the remote repository

After you make some changes to the SDK, commit and push those changes to the remote repository in your fork project.

**To push the changes to the remote repository, follow these steps:**

1. Check out the branch by entering the following command:

    ```shell
        git checkout username/my-branch-name
    ```

2. Commit your changes by entering the following command:

    ```shell
        git commit -am “My commit message”
    ```

    {{<tip "Tip">}}
Make sure your commit messages adhere to the project's [Best practices for commit messages]({{< ref "/contributing/contribution-best-practices.md#best-practices-for-commit-messages" >}}).

For example:

 ```shell
        git commit -am "Include flatpaks fontconfig configuration"
```

{{</tip>}}

1. Rebase your changes onto the target branch by entering the following command:

    ```shell
        git rebase origin/specific-release-branch
    ```

2. Push the changes to the remote repository by entering the following command:

    ```shell
        git push --set-upstream origin my-branch-name
    ```

    {{<tip "Tip">}}
If you want to commit and rebase changes to a branch that is already available on the remote, you may need to append the push command with `-f` to force-push the branch.
    {{</tip>}}

## Creating a merge request

If you make changes in your fork and want to push those changes to the upstream Freedesktop SDK project, you need to create a merge request.

For more information about creating GitLab merge requests, see the [GitLab documentation](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html).

- As a source branch, select the feature branch in your fork.
- As the target branch, select the branch in the upstream project that you want to update, for example, `master`.

After the merge request passes the CI checks, one of the project developers reviews the submitted changes. With the request approved, the merge bot rebases your changes onto the latest commit of the target branch, until the merge is complete.

{{<note "Note">}}
Your merge request can stay in review for up to six months. After that time, if there are no blockers, the project developer closes the merge request.
{{</note>}}

## Requesting developer access

If you are a long-term contributor to Freedesktop SDK, you can request developer access to the project to simplify the contribution process. With the developer access, you can push your changes directly in the project and then create a merge request for approval.

Developer access also lets you use the CI pipelines that have runners for the following architectures:

- `aarch64`
- `ppc64le`
- `i686`
- `x86_64`

**To request developer access, follow these steps:**

1. Go to the [GitLab page for the Freedesktop SDK project](https://gitlab.com/freedesktop-sdk/freedesktop-sdk).
2. At the top of the page, select **Request Access**.

One of the project maintainers can review and approve your request.
