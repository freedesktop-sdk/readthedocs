---
title: "Contributing to the SDK"
linkTitle: "Contributing"
infoType: hub
menu: primary
icon: contributing
weight: 5
---

Learn how your contributions can help to improve the Freedesktop SDK software integration project.

<!--more-->

You can contribute to the project in the following ways:

- Testing and debugging.
- Automating manual tasks in the project.
- Writing code that adds new functionality to the project.
- Improving or expanding the current features.
- Creating useful workarounds.
- Expanding or improving the project documentation.
- Sending suggestions, bug reports, and requests for new features or changes.
- Help integrate new components.
- Help with reviews.

Any contributions in these areas are welcome because they help make Freedesktop SDK more useful and beneficial for the community.

## Contact and support

For questions, problems, or suggestions about Freedesktop SDK, contact the project developers in one of the following ways:

- [Matrix communication platform](https://matrix.to/#/#freedesktop-sdk:matrix.org)
- [Mailing list](https://lists.freedesktop.org/mailman/listinfo/freedesktop-sdk)
