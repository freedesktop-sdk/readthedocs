---
title: "Freedesktop SDK guide"
infoType: hub
weight: 1
hideBreadcrumbs: true
---

Welcome to the Freedesktop SDK documentation. Freedesktop SDK is a free, community-developed, and open-source project with a number of components that help you simplify the process of creating different software artifacts. This can be containers, flatpaks, snaps or complete operating systems.
<!--more-->
