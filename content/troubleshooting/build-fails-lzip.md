---
title: "Build fails (lzip)"
weight: 2
infoType: troubleshooting
icon: troubleshooting
---
<!--more-->
## Symptom

The Freedesktop SDK build process displays the following error:

```console
tar source at elements/include/gettext-source.yml [line 2 column 2]: Did not find 'lzip' in PATH
```

## Cause

Your configuration is missing the `lzip` tool, which BuildStream uses to decompress certain tarballs.

## Remedy

Install the missing tool:

```shell
sudo apt install lzip
```
