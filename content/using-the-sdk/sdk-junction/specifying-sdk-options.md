---
title: "Specifying the SDK options"
weight: 2
infoType: task
---

Learn specify and use the Freedesktop SDK options in a junction within your own project.

<!--more-->

When you use Freedesktop SDK as a junction in your project, BuildStream does not automatically pass through the Freedesktop SDK project options. In some situations these may not have a default value and therefore should be defined in your junction element or passes through so they can be set as an option in your own project.

For example, the [`gnome-build-meta`](https://gitlab.gnome.org/GNOME/gnome-build-meta/blob/master/elements/freedesktop-sdk.bst) project defines Freedesktop SDK as a junction and specifically sets the `target_arch` and `bootstrap_build_arch` based the variable `arch` which is defined in the GNOME project [`project.conf`](https://gitlab.gnome.org/GNOME/gnome-build-meta/-/blob/master/project.conf) file.

{{<tip "Prerequisites">}}

- Make sure you have an existing BuildStream project.
    - For more information about configuring BuildStream projects, see the [BuildStream documentation](https://docs.buildstream.build/2.0/tutorial/first-project.html).
- Create a junction element for Freedesktop SDK.
    - For more information, see [Using the SDK as a junction]({{< ref "/using-the-sdk/sdk-junction/_index.md" >}}).
{{</tip>}}

**To forward the options from Freedesktop SDK junction, follow these steps:**

Freedesktop SDK can be built with various options. In your project, which junctions from Freedesktop SDK, you may want to allow the user to select these options when building an element and have them passed to Freedesktop SDK. Alternatively you may want these options to be set to specific values. See also the [BuildStream documentation](https://docs.buildstream.build/master/elements/junction.html#options) for this feature.

1. In your BuildStream elements directory, open your `.bst` junction element.
2. In the junction element, add the specific Freedesktop SDK options you either wish to pass through or define, as seen in the following example:

    ```yaml
    kind: junction

        sources:
        - kind: git_repo
          url: gitlab:freedesktop-sdk/freedesktop-sdk.git

        options:
            target_arch: "%{my_target_arch}"
            snap_grade: "stable"
    ```

    In the example, the option `snap_grade` is set to "stable" this is fixed and can only be changed by editing this line. The other option `target_arch` is deriving its value from a variable in your project called `my_target_arch`. Lets add `my_target_arch` into the `project.conf` of your project so it can be passed in as an option.

3. Save the `.bst` junction element.
4. Open the `project.conf` file.
5. In this file, add an option to set the variable `my_target_arch`, as seen in the following example:

    ```yaml
    name: my-super-cool-project

    min-version: 2.2

    element-path: elements

    options:
        freedesktop_sdk_target_arch:
        type: arch
        description: Freedesktop SDK Target Architecture
        variable: my_target_arch
        values:
        - aarch64
        - x86_64
    ```

    In the example, an option called `freedesktop_sdk_target_arch` is defined which stores its value to the same variable that the freedesktop SDK junction uses. Allowing setting this option when doing a build (`bst --option freedesktop_sdk_target_arch aarch64 build`). Another thing to note is how here we have limited the choice to just two values, instead of allowing all the values of `target_arch` which Freedesktop SDK supports we have limited these to only a subset which we support within this project.

6. Save the `project.conf` file.
