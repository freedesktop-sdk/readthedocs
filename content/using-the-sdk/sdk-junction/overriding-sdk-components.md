---
title: "Overriding the SDK components"
weight: 1
infoType: task
---

Learn how to override the Freedesktop SDK components and use components from your own project instead.

<!--more-->

When you use Freedesktop SDK as a junction in your project, BuildStream fetches all Freedesktop SDK components and makes them available for use by your project. In some situations, you may want to use these components, but with different configurations that better match the requirements of your project.

For example, the [`gnome-build-meta`](https://gitlab.gnome.org/GNOME/gnome-build-meta/blob/master/elements/freedesktop-sdk.bst) project defines Freedesktop SDK as a junction but overrides some Freedesktop SDK components with specific GNOME project components.

{{<tip "Prerequisites">}}

- Make sure you have an existing BuildStream project.
    - For more information about configuring BuildStream projects, see the [BuildStream documentation](https://docs.buildstream.build/2.0/tutorial/first-project.html).
- Create a junction element for Freedesktop SDK.
    - For more information, see [Using the SDK as a junction]({{< ref "/using-the-sdk/sdk-junction/_index.md" >}}).
{{</tip>}}

**To override Freedesktop SDK components, follow these steps:**

1. In your BuildStream elements directory, open your `.bst` junction element.
2. In the junction element, override the specific Freedesktop SDK components with other components, as in the following example:

    ```yaml
    kind: junction

        sources:
        - kind: git_repo
          url: gitlab:freedesktop-sdk/freedesktop-sdk.git

        overrides:
            components/appstream-glib.bst: sdk/appstream-glib.bst
    ```

    In the example, the `overrides` dictionary lists the Freedesktop SDK component to override (`components/appstream-glib.bst`), and then provides the component to use instead (`sdk/appstream-glib.bst`). In short, the `overrides` dictionary uses the following syntax:

    ```yaml
    overrides:
        component-to-override: component-to-use-instead
    ```

3. Save the `.bst` junction element.
