---
title: "Building Flatpak runtimes using the SDK"
linkTitle: "Building Flatpak runtimes"
weight: 2
infoType: task
icon: task
---
Learn how to build Flatpak runtimes available in Freedesktop SDK.

<!--more-->
With Flatpak, you can package and distribute applications across different Linux distributions. The Freedesktop SDK Flatpak runtimes are available on Flathub, but you can also build the runtimes locally, using the Freedesktop SDK components.

{{<tip "Prerequisites">}}

- Install and configure [BuildStream](https://buildstream.build/).
    - Use BuildStream version 2.
    - For more information about using BuildStream, see the [BuildStream documentation](https://docs.buildstream.build/2.0/index.html).
- Install and configure [GNU Make](https://www.gnu.org/software/make/).
- Make sure you have [Flatpak](https://flatpak.org/) installed.
{{</tip>}}

## Installing Flatpak runtimes from Flathub

You can install the Freedesktop SDK and Platform runtimes directly from [Flathub](https://flathub.org/en).

**To install a Flatpak runtime from Flathub, follow these steps:**

1. Add the Freedesktop SDK Flatpak remote repository from Flathub to your Flatpak configuration by entering the following command:

    ```shell
    flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
    ```

2. Install the runtimes:

   - To install the SDK runtime, enter the following command:

        ```shell
        flatpak install flathub org.freedesktop.Sdk//23.08
        ```

   - To install the Platform runtime, enter the following command:

        ```shell
        flatpak install flathub org.freedesktop.Platform//23.08
        ```

## Building the Freedesktop SDK Flatpak runtime

The default `make build` command builds the majority of Freedesktop SDK outputs, including the Flatpak runtimes. This process can be time-consuming, so for building only the Flatpak runtimes, the recommended approach is to use the `make export` command.

**To build a Flatpak runtime, follow these steps:**

1. Open your preferred terminal and go to the root of the `freedesktop-sdk` repository.
2. Build and export the Flatpak runtime by entering the following command:

    ```shell
    make export
    ```

    {{<note "Note">}}
The default location for the output is the `repo` directory in the `freedesktop-sdk` root.
    {{</note>}}

3. Add the Freedesktop SDK Flatpak remote repository to your Flatpak configuration by entering the following command:

    ```shell
    flatpak remote-add --user --no-gpg-verify local $PWD/repo
    ```

    {{<note "Note">}}
The commands adds the remote repository named `local` to the active user's (`--user`) Flatpak configuration and disables the signature verification (`--no-gpg-verify`) for the repository packages.
    {{</note>}}

4. Install the runtimes:

   - To install the SDK runtime, enter the following command:

        ```shell
        flatpak install local org.freedesktop.Sdk//23.08
        ```

   - To install the Platform runtime, enter the following command:

        ```shell
        flatpak install local org.freedesktop.Platform//23.08
        ```

    {{<tip "Tip">}}
When you install the runtimes with these commands, replace the `23.08` string with the string for the version that you built.
    {{</tip>}}

### Runtime extensions and test applications

Freedesktop SDK provides various useful `org.freedesktop.Platform` and `org.freedesktop.Sdk` runtimes extensions, as well as some Flatpak applications for GPU testing.

For example, to show all available runtime and applications for the `local` remote repository, enter the following command:

```shell
flatpak remote-ls local
```

## Building Flatpak runtimes in a downstream project

You can use the Freedesktop SDK components to help build Flatpak runtimes in your own BuildStream project.

For example, the [`gnome-build-meta`](https://gitlab.gnome.org/GNOME/gnome-build-meta) project uses `freedesktop-sdk` as a junction to help build various outputs, including Flatpak runtimes.

In that project, the [`freedesktop-sdk.bst`](https://gitlab.gnome.org/GNOME/gnome-build-meta/-/blob/3451db33f725bf2379f93f61a58b8d19efa718c9/elements/freedesktop-sdk.bst) file defines Freedesktop SDK as a junction:

```yaml
kind: junction

sources:
- kind: git_repo
  url: gitlab:freedesktop-sdk/freedesktop-sdk.git
  track: freedesktop-sdk-23.08*
  ref: freedesktop-sdk-23.08.3-0-gd58cbd03841cdf4d914151c5970c1695395240a7
...
```

Another element, [`platform.bst`](https://gitlab.gnome.org/GNOME/gnome-build-meta/-/blob/3451db33f725bf2379f93f61a58b8d19efa718c9/elements/flatpak/platform.bst), uses the [`flatpak_image`](https://buildstream.gitlab.io/bst-plugins-experimental/elements/flatpak_image.html) BuildStream plugin to define the contents of a Platform Flatpak runtime:

```yaml
kind: flatpak_image
build-depends:
- flatpak/platform-image.bst
- freedesktop-sdk.bst:integration/platform-integration.bst

variables:
  # Obtain the versions of the flatpak extensions
  (@): freedesktop-sdk.bst:include/repo_branches.yml

config:
  directory: '%{prefix}'
  exclude:
  - debug
  - docs
  - locale

  metadata:
    Runtime:
      name: org.gnome.Platform
      runtime: org.gnome.Platform/%{gcc_arch}/%{flatpak-branch}
      sdk: org.gnome.Sdk/%{gcc_arch}/%{flatpak-branch}
    ...
```

{{<note "Note">}}
To use Flatpak metadata in BuildStream, the metadata must be converted to YAML.
For more information about `flatpak-metadata`, see the [Flatpak documentation](https://docs.flatpak.org/en/latest/flatpak-command-reference.html#flatpak-metadata).
{{</note>}}

Then, the [`flatpak-runtimes.bst`](https://gitlab.gnome.org/GNOME/gnome-build-meta/-/blob/3451db33f725bf2379f93f61a58b8d19efa718c9/elements/flatpak-runtimes.bst) element uses the [`flatpak_repo`](https://buildstream.gitlab.io/bst-plugins-experimental/elements/flatpak_repo.html) BuildStream plugin and defines how to deploy the contents of various Flatpak runtimes, including the one defined in the `platform.bst` element:

```yaml
kind: flatpak_repo

build-depends:
- freedesktop-sdk.bst:components/flatpak.bst
- filename:
  - flatpak/platform.bst
  - flatpak/platform-locale.bst
  - flatpak/sdk.bst
  - flatpak/sdk-debug.bst
  - flatpak/sdk-docs.bst
  - flatpak/sdk-locale.bst
  config:
    flatpak-image: true

config:
  arch: '%{gcc_arch}'
  branch: '%{flatpak-branch}'
```

{{<note "Note">}}
If you're adding Flatpak images as dependencies, set `flatpak-image: true`.

If you're adding Flatpak images to a stack before adding them as dependencies, set `flatpak-stack: true`.
{{</note>}}

The project's [CI build configuration](https://gitlab.gnome.org/GNOME/gnome-build-meta/-/blob/3451db33f725bf2379f93f61a58b8d19efa718c9/.gitlab-ci.yml#L58-77) contains BuildStream commands, which are responsible for building specific runtime elements, for example the `flatpak-runtimes.bst` element.

{{<tip "Tip">}}
To build the element responsible for deploying the runtime, you can use the `bst build <element>` command. The `<element>` file should be the one that uses the `flatpak_repo` plugin.
{{</tip>}}
