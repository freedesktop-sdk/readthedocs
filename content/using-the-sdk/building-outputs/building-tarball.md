---
title: "Building tarballs using the SDK"
linkTitle: "Building tarballs"
weight: 5
infoType: task
icon: task
---

Learn how to create a tarball from the Freedesktop SDK components.

<!--more-->
BuildStream natively supports generating tarballs. The `Makefile` in Freedesktop SDK contains commands with which you can build a stack of elements, check out the resulting artifacts as a tarball.

An example situation in which you may want to create a tarball is when you need to provide a third party with a compiling toolchain, which they can use without any special tools.
With Freedesktop SDK, you can create a tarball archive that contains all components necessary for compiling.
Then, you can share the tarball with others so that they can compile their stack against your binaries and ensure compatibility with your own system.

{{<tip "Prerequisites">}}

- Install and configure [BuildStream](https://buildstream.build/).
    - Use BuildStream version 2.
    - For more information about using BuildStream, see the [BuildStream documentation](https://docs.buildstream.build/2.0/index.html).
- Install and configure [GNU Make](https://www.gnu.org/software/make/).
- Make sure you have `tar` installed.
    - For more information about BuildStream dependencies, see the [BuildStream dependencies documentation](https://docs.buildstream.build/master/main_install.html#installing-dependencies).
{{</tip>}}

**To build a tarball, follow these steps:**

1. Open your preferred terminal and go to the root of the `freedesktop-sdk` repository.
2. Build tarballs for the SDK and Platform elements by entering the following command:

   ```shell
   make build-tar
   ```

    {{<note "Note">}}
If you want to build a tarball for only one of the elements, append the command with either the `TARBALLS=platform` or `TARBALLS=sdk` parameter.
{{</note>}}

3. Check out the build artifacts and compress them into a tarball by entering the following command:

   ```shell
   make export-tar
   ```

    {{<note "Note">}}
If you want to create an archive for only one of the elements, append the command with either the `TARBALLS=platform` or `TARBALLS=sdk` parameter.
{{</note>}}

The process adds the resulting `.tar.xz` archives to the architecture-specific folder in the `elements/tarballs` directory. By default, the name of the archives folder mirrors the architecture of the current machine, for example `elements/tarballs/x86_64-platform`.
