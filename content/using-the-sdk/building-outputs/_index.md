---
title: "Building outputs using the SDK"
linkTitle: "Building outputs"
infoType: hub
icon: task
weight: 3
---
With Freedesktop SDK, you can build various outputs from the same set of components.

<!--more-->

Freedesktop SDK relies on BuildStream to build outputs. BuildStream has an architecture based on plugins that allows you to create different outputs, such as Flatpak or Snap.

For more information about BuildStream plugins, see the [BuildStream documentation](https://docs.buildstream.build/2.0/core_plugins.html).

With Freedesktop SDK, you can build the following outputs:

| Output | Description |
| ------------- | ------------- |
| **Operating system image** | Build sample various OS images that you can use as a starting point for your own OS and run in a virtualisation tool. |
| **Flatpak runtime** | Build Platform and SDK runtimes for Flatpak applications, as well as sample Flatpak applications that you can use for GPU testing. |
| **OCI image** | Build OCI standard-compliant images that are easy to integrate with other tools and environments. Freedesktop SDK and BuildStream guarantee reproducibility, ensuring the consistency of subsequent OCI image builds. |
| **Snap runtime** | Build a distribution-agnostic Snap runtime for creating Linux-based applications. By default, Snap uses a Ubuntu-based runtime, which Freedesktop SDK can replace. |
| **Tarball** | Build `tar` archives from binaries available in Freedesktop SDK. |
