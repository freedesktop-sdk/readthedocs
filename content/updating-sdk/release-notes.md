---
title: "Release notes"
infoType: reference
icon: release-notes-1
weight: 1
---
Freedesktop SDK has a major release every year, and each release has a maintenance period of two years.

<!--more-->
The following table contains release information for the latest versions of Freedesktop SDK:

| Release date | Release name | End of life | Notes |
| ------------- | ------------- | ------------- | ------------- |
| September 8, 2024 | 24.08 Final Stable | August 30, 2026  | [`NEWS`](https://gitlab.com/freedesktop-sdk/freedesktop-sdk/-/blob/release/24.08/NEWS.yml) |
| August 30, 2023 | 23.08 Final Stable | August 30, 2025  | [`NEWS`](https://gitlab.com/freedesktop-sdk/freedesktop-sdk/-/blob/release/23.08/NEWS.yml) |
| August 30, 2022 | 22.08 Final Stable | August 30, 2024  | [`NEWS`](https://gitlab.com/freedesktop-sdk/freedesktop-sdk/-/blob/release/22.08/NEWS.yml) |
| August 31, 2021 | 21.08 Final Stable | August 30, 2023  | [`NEWS`](https://gitlab.com/freedesktop-sdk/freedesktop-sdk/-/blob/release/21.08/NEWS) |
