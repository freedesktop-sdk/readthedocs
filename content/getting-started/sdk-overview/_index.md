---
title: "SDK overview"
weight: 1
infoType: overview
icon: release-notes-2
---
Freedesktop SDK consists of a group of software definitions that you can integrate together to create various outputs, such as Flatpak runtimes or operating systems.

<!--more-->
The main purpose of the Freedesktop SDK project is to create a lightweight, flexible, and multipurpose runtime with essential features and a shared set of dependencies.

Examples of projects that use Freedesktop SDK:

- [GNOME OS](https://os.gnome.org/)
- [carbonOS](https://carbon.sh/)
- [Flatpak](https://flatpak.org/)

## Benefits

There are several benefits of using Freedesktop SDK:

- Simple and declarative format of software definitions.
- Ability to create complex outputs with one tool:
    - Flatpak runtimes.
    - Container images.
    - Operating systems.
- Usage of industry best practices for building software, including sandboxing, and reproducibility.
- Minimal binary that promotes safety and security, without dependencies to other projects.
- Support for different desktop environments, Linux distributions, and architectures, which promotes interoperability and consistency.
