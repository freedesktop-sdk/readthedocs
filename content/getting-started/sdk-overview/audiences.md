---
title: "Audiences"
weight: 1
infoType: concept
---
This guide provides information for developers who want to use the SDK to build software and for SDK developers who want to contribute to the project.

<!--more-->
Developers who use Freedesktop SDK to build software can use this guide for the following:

- Learn about the capabilities of the SDK that can be useful for software development.
- Learn about the best practices for using the SDK.
- Learn how to make the SDK part of their development stack.

For more information on using the SDK, see [Using the SDK]({{< ref "/using-the-sdk/_index.md" >}}).

For more information on contributing to the SDK, see [Contributing to the SDK]({{< ref "/contributing/_index.md" >}}).
