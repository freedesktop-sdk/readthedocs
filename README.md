# Welcome to the Freedesktop SDK documentation

The [Freedesktop SDK documentation portal](https://freedesktop-sdk.gitlab.io/documentation/) provides an overview of the SDK and focuses on its major use cases.

## Contributing to the documentation

If you want to help with creating and updating the Freedesktop SDK documentation, you can follow the [same workflow](https://freedesktop-sdk.gitlab.io/documentation/contributing/making-changes/) as the standard SDK project contributors. When you create a merge request, you can preview your changes with the `View app` button:

![Merge request preview](static/mr-preview.png)

If you intend to make frequent contributions to the documentation, we encourage you to request developer access to this repository.

**Note:** Make sure to install [Git LFS](https://git-lfs.com/), which the project uses to manage images.

### Before you begin

The documentation portal is based on the [Hugo](https://gohugo.io/) static site generator and uses some specific tools for local testing and content validation.

If you want to take advantage of the project's content validation and local testing features, install the following:

- [Visual Studio Code](https://code.visualstudio.com/download)
- [Node.js](https://nodejs.org/en/download) (with required tools)
- [Hugo](https://gohugo.io/installation/)
- [Vale](https://vale.sh/docs/vale-cli/installation/)
- [YQ](https://github.com/mikefarah/yq#install)

After you open the local copy of the documentation repository in Visual Studio Code, do the following:

1. Install all recommended extensions when prompted.
2. Install the `package.json` file in `themes\3di` using `npm install ` or Visual Studio Code's `NPM Scripts` pane.

The following folders are important for content creation:

- `.vale` — folder with the Vale prose linter configuration.
- `.vscode` — folder with the Visual Studio Code configuration.
- `content` — folder with the Markdown files for the documentation portal.
- `static` — folder for Hugo static files, such as extra resources and images.

### Creating content

The project uses the [CommonMark](https://commonmark.org/) standard with GitHub Flavored Markdown. If you want to create new content, we recommend opening some existing files and using them as templates.

While you can use the basic Markdown syntax, the project uses Hugo shortcodes for the following:

- Cross-references to other files

    ```md
    [Sample]({{< ref "/path-to-file/file.md" >}})
    ```

- Images (by default stored in `/static`)

    ```md
    {{< figure src="image.png" alt="Alternate text" >}}
    ```

- Notices:

    ```md
    {{<tip "Tip">}}
    Sample tip.
    {{</tip>}}
    ```

    ```md
    {{<note "Note">}}
    Sample note.
    {{</note>}}
    ```

### Validating content

We use [Vale](https://vale.sh/) and [markdownlint](https://github.com/DavidAnson/vscode-markdownlint) to enforce some authoring best practices. The `.vale` folder contains the rules and dictionaries that act as guardrails for content creation.

While the GitLab project's CI configuration defines Vale and Markdown validation jobs, you can also validate your content locally. With Vale [installed](https://vale.sh/docs/vale-cli/installation/), do the following:

1. Open your preferred terminal and go to the root of the documentation repository.
2. Validate the `content` folder by entering the following command:

    ```bash
    vale --config .vale.ini content
    ```

This command reports any Vale warnings, suggestions, and errors found in the `content` folder. Any errors present in the `content` folder result in a failed CI pipeline and prevent the content from building. In addition, when you're working on an open Markdown file, Vale runs in the background to provide live validation for your content.

### Local testing

With [Hugo](https://gohugo.io/installation/) installed, you can launch a local version of the documentation portal to verify your changes.

1. Open your preferred terminal and go to the root of the documentation repository.
2. Download ```git lfs``` assets

    ```bash
    git lfs pull
    ```

3. Run the local Hugo server:

    ```bash
    hugo server --config hugo.yaml
    ```

    In Visual Studio Code, you can also run the server with the `Run Build Task` shortcut: `CTRL+SHIFT+B` (or `CMD+SHIFT+B` on Mac).

4. Copy the server address, for example `http://localhost:1313/documentation/`, and paste the address into your browser.
